import { useState } from 'react'
import reactLogo from './assets/react.svg'
import './App.css'
import {Button} from "@mui/material";
import React from 'react';
import {Route, Routes} from "react-router-dom";
import Home from "./containers/Home/Home";

function App() {
  const [count, setCount] = useState(0)

  return (
    <div className="App">
        <Routes>
            <Route path="/" element={<Home />} />
{/*            <Route path="about" element={<About />} />*/}
        </Routes>
    </div>
  )
}

export default App
