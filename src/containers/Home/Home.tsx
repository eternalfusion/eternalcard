import React, {FunctionComponent} from 'react'

import { useTheme } from "@mui/material/styles"
import reactLogo from "../../assets/react.svg";
import {Button} from "@mui/material";

type HomeProps = {}

const Home: FunctionComponent<HomeProps> = ({}) => {
    const theme = useTheme()

    const styles = {
        header: {
            backgroundColor: 'primary.main',
            mb: '1.45rem' // margin bottom
        },
        link: {
            color: 'primary.contrastText',
            textDecoration: 'none'
        },
        title: {
            my: 0,  // top/bottom margin
            mx: 'auto', // left/right margin
            maxWidth: 960,
            py: '1.45rem', // top/bottom padding
            px: '1.0875rem' // left/right padding
        },
        customComponent: { // to be used with none MUI components
            backgroundColor: theme.palette.primary.main
        }
    }

    return (
        <div>
            <div>
                <a href="https://reactjs.org" target="_blank">
                    <img src={reactLogo} className="logo react" alt="React logo" />
                </a>
            </div>
            <h1>React!</h1>
            <div className="card">
                <p>
                    Edit <code>src/App.tsx</code> and save to test HMR
                </p>
            </div>
            <p className="read-the-docs">
                Click on the Vite and React logos to learn more
            </p>
            <Button sx={{ background: 'linear-gradient(63deg, rgba(154,18,101,1) 35%, rgba(0,143,255,1) 100%)', borderRadius: 5 }}>
                test
            </Button>
        </div>
    );
}
export default Home;
